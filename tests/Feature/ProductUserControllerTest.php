<?php

namespace Tests\Feature;

use Mockery;
use Carbon\Carbon;
use Tests\TestCase;
use App\Models\User;
use App\DTO\User\UserDTO;
use App\Models\Mysql\Product;
use App\DTO\Product\ProductDTO;
use Illuminate\Http\JsonResponse;
use App\DTO\Product\ProductRentDTO;
use Illuminate\Support\Facades\Auth;
use Spatie\LaravelData\DataCollection;
use App\DTO\ProductUser\ProductUserDTO;
use Illuminate\Contracts\Validation\Validator;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Controllers\ProductUserController;
use App\Repositories\Product\ProductRepository;
use App\Repositories\ProductUser\ProductUserRepository;
use App\Http\Requests\Client\Product\RenewalRentRequest;

class ProductUserControllerTest extends TestCase
{

    /**
     * @var \Illuminate\Contracts\Validation\Validator
     */
    private Validator $validator;

    /**
     * @var \App\Models\User|\App\DTO\User\UserDTO
     */
    private User|UserDTO $user;

    /**
     * @var \Spatie\LaravelData\DataCollection
     */
    private DataCollection $products;

    /**
     * @var \App\Repositories\ProductUser\ProductUserRepository
     */
    private ProductUserRepository $productUserRepositoryMock;
    public function setUp(): void
    {
        parent::setUp();
        $this->validator = Mockery::mock(Validator::class);
        $this->validator->shouldReceive('fails')->andReturn('false');

        $validatedData = [
            'rent_time_period' => 4
        ];

        $this->user = User::factory()
            ->count(1)
            ->create()
            ->first();

        $this->validator->shouldReceive('validated')->andReturnUsing(function ($key = null) use ($validatedData) {
            return $key === null ? $validatedData : $validatedData[$key] ?? null;
        });

        Auth::shouldReceive('id')->andReturn($this->user->id);

        $this->products = ProductRentDTO::collection(
            Product::factory()
                ->count(10)
                ->create()
                ->toArray()
        );

        $this->productUserRepositoryMock = Mockery::mock(ProductUserRepository::class);

    }

    /**
     * Test проверяет метод который возвращает купленные пользователем продукты
     *
     * @return void
     */
    public function testUserBoughtProducts()
    {

        $this->productUserRepositoryMock->shouldReceive('getBoughtProducts')
            ->once()
            ->andReturn(
                ProductUserDTO::collection([
                    ProductUserDTO::from([
                        'user_id' => $this->user->id,
                        'product_id' => $this->products->first()->id,
                        'rental_time' => null
                    ])
                ])
            );

        $controller = new ProductUserController($this->productUserRepositoryMock);

        $response = $controller->userBoughtProducts();


        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(Response::HTTP_OK, $response->status());

        $responseData = json_decode($response->content(), true);
        $this->assertArrayHasKey('bought_products', $responseData);

        $this->assertGreaterThanOrEqual(1, $responseData['bought_products']);
    }

    /**
     * Тест проверяет арендованные пользователем проудкты
     *
     * @return void
     */
    public function testUserRentProducts()
    {

        $this->productUserRepositoryMock->shouldReceive('getRentProducts')
            ->once()
            ->andReturn(
                $this->products
            );

        $controller = new ProductUserController($this->productUserRepositoryMock);

        $response = $controller->userRentProducts();


        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(Response::HTTP_OK, $response->status());

        $responseData = json_decode($response->content(), true);
        $this->assertArrayHasKey('rent_products', $responseData);

        $this->assertGreaterThanOrEqual(1, $responseData['rent_products']);
    }

    /**
     * @throws \App\Exceptions\DatabaseException
     * @throws \App\Exceptions\ValidationException
     */
    public function testRenewalRent()
    {

        $controller = new ProductUserController($this->productUserRepositoryMock);

        $request = (new RenewalRentRequest())->setValidator($this->validator);

        $product = $this->products->first();
        $product->rent_period_price = 5;

        $mockerProductUser = ProductUserDTO::from([
            'id' => 1,
            'user_id' => $this->user->id,
            'product_id' => $product->id,
            'product' => ProductRentDTO::from($product),
            'user' => UserDTO::from($this->user),
            'rental_time' => Carbon::now()->addSeconds(30)->format('Y-m-d H:i:s')
        ]);


        $this->productUserRepositoryMock->shouldReceive('findWithProductAndRentPeriodPrice')
            ->once()
            ->andReturn($mockerProductUser);


        $response = $controller->renewalRent($request, $mockerProductUser->id);

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(Response::HTTP_OK, $response->status());

        $responseData = json_decode($response->content(), true);
        $this->assertArrayHasKey('product_user', $responseData);
    }

}
