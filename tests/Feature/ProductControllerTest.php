<?php

namespace Tests\Feature;

use Mockery;
use Tests\TestCase;
use App\Models\User;
use App\Models\Mysql\Product;
use App\DTO\Product\ProductDTO;
use App\DTO\Product\ProductRentDTO;
use Illuminate\Support\Facades\Auth;
use Spatie\LaravelData\DataCollection;
use App\Repositories\User\UserRepository;
use App\Http\Controllers\ProductController;
use Symfony\Component\HttpFoundation\Response;
use App\Exceptions\InsufficientFundsException;
use Illuminate\Contracts\Validation\Validator;
use App\Repositories\Product\ProductRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Http\Requests\Client\Product\RentProductRequest;

class ProductControllerTest extends TestCase
{

    use DatabaseTransactions;

    /**
     * @var \App\Http\Controllers\ProductController
     */
    private ProductController $controller;

    /**
     * @var DataCollection
     */
    private DataCollection $products;

    /**
     * @var \App\Repositories\User\UserRepository
     */
    private UserRepository $userRepositoryMock;

    /**
     * @var \App\Repositories\Product\ProductRepository
     */
    private ProductRepository $productRepositoryMock;

    /**
     * @var \App\Models\User
     */
    private User $user;

    /**
     * @var \Illuminate\Contracts\Validation\Validator
     */
    private Validator $validator;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->validator = Mockery::mock(Validator::class);
        $this->validator->shouldReceive('fails')->andReturn('false');

        $validatedData = [
            'rent_time_period' => 4
        ];

        $this->validator->shouldReceive('validated')->andReturnUsing(function ($key = null) use ($validatedData) {
            return $key === null ? $validatedData : $validatedData[$key] ?? null;
        });

        $this->products = ProductDTO::collection(
            Product::factory()
                ->count(10)
                ->create()
                ->toArray()
        );

        $this->user = User::factory()
            ->count(1)
            ->create()
            ->first();

        Auth::shouldReceive('id')->andReturn($this->user->id);

        $this->productRepositoryMock = Mockery::mock(ProductRepository::class);
        $this->userRepositoryMock = Mockery::mock(UserRepository::class);

        $this->controller = new ProductController(
            $this->productRepositoryMock,
            $this->userRepositoryMock
        );
    }

    /**
     * @return void
     */
    public function testIndexActive()
    {
        $this->productRepositoryMock->shouldReceive('listActiveWithPaginate')
            ->once()
            ->andReturn(
                $this->products
            );

        $response = $this->controller->indexActive();
        $responseData = json_decode($response->content(), true);


        $this->assertEquals(Response::HTTP_OK, $response->status());

        $this->assertArrayHasKey('products', $responseData);

        $this->assertGreaterThanOrEqual(10, $responseData['products']);
    }

    /**
     * @throws \App\Exceptions\DatabaseException
     * @throws \App\Exceptions\InsufficientFundsException
     */
    public function testBuySuccess()
    {
        $this->productRepositoryMock->shouldReceive('find')
            ->once()
            ->andReturn(
                $this->products->first()
            );

        $this->userRepositoryMock->shouldReceive('lockForUpdate')
            ->once()
            ->andReturn(
                $this->user
            );


        $response = $this->controller->buy($this->products->first()->id);

        $responseData = json_decode($response->content(), true);

        $this->assertEquals(Response::HTTP_OK, $response->status());

        $this->assertArrayHasKey('product_user', $responseData);
    }

    /**
     * @return void
     * @throws \App\Exceptions\DatabaseException
     * @throws \App\Exceptions\InsufficientFundsException
     */
    public function testBuyLowBalanceError()
    {
        $lowBalanceUser = User::factory()
            ->count(1)
            ->create()
            ->first();

        $lowBalanceUser->balance = 0;

        $lowBalanceUser->save();

        $this->productRepositoryMock->shouldReceive('find')
            ->once()
            ->andReturn(
                $this->products->first()
            );

        $this->userRepositoryMock->shouldReceive('lockForUpdate')
            ->once()
            ->andReturn(
                $lowBalanceUser
            );

        $this->expectException(InsufficientFundsException::class);

        $this->controller->buy($this->products->first()->id);
    }

    /**
     * @throws \App\Exceptions\DatabaseException
     * @throws \App\Exceptions\ValidationException
     * @throws \App\Exceptions\InsufficientFundsException
     */
    public function testRentSuccess()
    {
        $this->userRepositoryMock->shouldReceive('lockForUpdate')
            ->once()
            ->andReturn(
                $this->user
            );

        $product = Product::factory()->count(1)->create()->first();

        $product->setAttribute('rent_period_price',5);

        $this->productRepositoryMock->shouldReceive('findWithRentPeriod')
            ->once()
            ->andReturn(
                ProductRentDTO::from($product)
            );

        $response = $this->controller->rent(
            productId: Product::factory()->count(1)->create()->first()->id,
            request: (new RentProductRequest())->setValidator($this->validator)
        );

        $this->assertEquals(Response::HTTP_OK, $response->status());

        $responseData = json_decode($response->content(), true);
        $this->assertArrayHasKey('product_user', $responseData);


    }

}
