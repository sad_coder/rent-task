<?php

namespace App\Models\Mysql;

use Illuminate\Database\Eloquent\Model;

class TransactionLog extends Model
{

    /**
     * @var string[]
     */
    protected $guarded = ['id'];

}
