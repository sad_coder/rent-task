<?php

namespace App\Models\Mysql;

use Illuminate\Database\Eloquent\Model;

class ProductRenewalTimePeriod extends Model
{
    /**
     * @var string[]
     */
    protected $guarded = ['id'];
}
