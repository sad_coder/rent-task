<?php

namespace App\Models\Mysql;

use Illuminate\Database\Eloquent\Model;

class RentPeriod extends Model
{
    /**
     * @var string[]
     */
    protected $guarded = ['id'];
}
