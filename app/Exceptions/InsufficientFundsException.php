<?php

namespace App\Exceptions;

use Throwable;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class InsufficientFundsException extends BaseException
{
    /**
     * @var int
     */
    protected int $errorCode = 402;

    /**
     * @param string          $message
     * @param int             $code
     * @param \Throwable|null $previous
     */
    public function __construct(string $message = "", int $code = 0, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render() : JsonResponse
    {
        return response()->json(
            [
                'message' => 'the product cannot be purchased or your balance is insufficient'
            ],
            Response::HTTP_PAYMENT_REQUIRED,
        );
    }

}
