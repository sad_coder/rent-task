<?php

namespace App\Exceptions;

use Throwable;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class ProductNotActiveException extends BaseException
{
    /**
     * @var int
     */
    protected int $errorCode = 400;

    /**
     * @param string          $message
     * @param int             $code
     * @param \Throwable|null $previous
     */
    public function __construct(string $message = "", int $code = 0, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render() : JsonResponse
    {
        return response()->json(
            [
                'message' => 'the item is not available for purchase or lease'
            ],
            Response::HTTP_BAD_REQUEST,
        );
    }
}
