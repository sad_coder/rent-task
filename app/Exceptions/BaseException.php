<?php

namespace App\Exceptions;

use Exception;

/**
 * Class BaseException
 *
 * @package App\Exceptions
 */
abstract class BaseException extends Exception
{
    /**
     * Exception error type constants
     */
    public const TYPE_DATABASE_ERROR = 'database_error';
    public const TYPE_VALIDATION_ERROR = 'validation_error';
    public const TYPE_UNIDENTIFIED_ERROR = 'unidentified_error';

    /**
     * Exception appearances constants
     */
    public const APPEARANCE_CORE = 'core-service';

    /**
     * @var string
     */
    protected string $explainErrorMessage;

    /**
     * @var int
     */
    protected int $errorCode;

    /**
     * @var string|null
     */
    protected ?string $systemErrorMessage;

    /**
     * @var string
     */
    protected string $errorAppearance;

    /**
     * @var string
     */
    protected string $errorType;

    /**
     * BaseException constructor
     *
     * @param string      $explainErrorMessage
     * @param int         $errorCode
     * @param string|null $systemErrorMessage
     * @param string      $errorAppearance
     * @param string      $errorType
     */
    public function __construct(
        string $explainErrorMessage,
        int $errorCode,
        ?string $systemErrorMessage = null,
        string $errorAppearance = self::APPEARANCE_CORE,
        string $errorType = self::TYPE_UNIDENTIFIED_ERROR
    )
    {
        $this->explainErrorMessage = $explainErrorMessage;
        $this->systemErrorMessage = $systemErrorMessage;
        $this->errorAppearance = $errorAppearance;
        $this->errorCode = $errorCode;
        $this->errorType = $errorType;

        parent::__construct(
            $explainErrorMessage,
            $errorCode
        );
    }

    /**
     * @return string
     */
    public function getExplainErrorMessage() : string
    {
        return $this->explainErrorMessage;
    }

    /**
     * @return int
     */
    public function getErrorCode() : int
    {
        return $this->errorCode;
    }

    /**
     * @return string|null
     */
    public function getSystemErrorMessage() : ?string
    {
        return $this->systemErrorMessage;
    }

    /**
     * @return string
     */
    public function getErrorAppearance() : string
    {
        return $this->errorAppearance;
    }

    /**
     * @return string
     */
    public function getErrorType() : string
    {
        return $this->errorType;
    }
}
