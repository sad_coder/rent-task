<?php

namespace App\Exceptions;

use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class ValidationException extends \Exception
{
    public function __construct(string $message = "", int $code = 0, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return JsonResponse
     */
    public function render() : JsonResponse
    {
        return response()->json(
            [
                'message' => __($this->message)
            ],
            Response::HTTP_UNPROCESSABLE_ENTITY,
        );
    }
}
