<?php

namespace App\DTO\TransactionLog;

use Spatie\LaravelData\Data;

class TransactionLogDTO extends Data
{
    /**
     * @param int|null $id
     * @param int      $product_id
     * @param int      $user_id
     * @param string   $type
     * @param string   $description
     */
    public function __construct(
        public ?int   $id,
        public int $product_id,
        public int $user_id,
        public string $type,
        public string $description,
    ) {}
}
