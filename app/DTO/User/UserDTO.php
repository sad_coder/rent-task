<?php

namespace App\DTO\User;


use Spatie\LaravelData\Data;

class UserDTO extends Data
{
    /**
     * @param int|null $id
     * @param string   $name
     * @param int      $balance
     */
    public function __construct(
        public ?int   $id,
        public string $name,
        public int $balance,
    ) {}
}
