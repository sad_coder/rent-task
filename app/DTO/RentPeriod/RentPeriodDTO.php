<?php

namespace App\DTO\RentPeriod;

use Spatie\LaravelData\Data;

class RentPeriodDTO extends Data
{
    public function __construct(
        public ?int $id,
        public string $name,
        public int $value,
        public ?int $pivot_price
    ) {
    }
}
