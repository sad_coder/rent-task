<?php

namespace App\DTO\Product;

use Spatie\LaravelData\Data;

class ProductDTO extends Data
{
    /**
     * @param int|null $id
     * @param int      $price
     * @param string   $name
     * @param bool     $is_active
     */
    public function __construct(
        public ?int   $id,
        public int $price,
        public string $name,
        public bool $is_active
    ) {}
}
