<?php

namespace App\DTO\Product;

use Spatie\LaravelData\Data;

class ProductRentDTO extends Data
{
    /**
     * @param int|null $id
     * @param int      $price
     * @param string   $name
     * @param bool     $is_active
     * @param int|null $rent_period_price
     */
    public function __construct(
        public ?int   $id,
        public int $price,
        public string $name,
        public bool $is_active,
        public ?int $rent_period_price
    ) {}
}
