<?php

namespace App\DTO\ProductUser;

use App\DTO\User\UserDTO;
use Spatie\LaravelData\Data;
use App\DTO\Product\ProductDTO;
use App\DTO\Product\ProductRentDTO;

class ProductUserDTO extends Data
{
    /**
     * @param int|null                             $id
     * @param string|null                          $rental_time
     * @param int                                  $product_id
     * @param int                                  $user_id
     * @param string|null                          $created_at
     * @param \App\DTO\Product\ProductRentDTO|null $product
     * @param \App\DTO\User\UserDTO|null           $user
     */
    public function __construct(
        public ?int $id,
        public ?string $rental_time,
        public int $product_id,
        public int $user_id,
        public ?string $created_at,
        public ?ProductRentDTO $product,
        public ?UserDTO $user,
        public ?string $code,
    ) {
    }
}
