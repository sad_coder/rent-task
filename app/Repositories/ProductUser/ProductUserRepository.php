<?php

namespace App\Repositories\ProductUser;

use Illuminate\Support\Str;
use Illuminate\Support\Carbon;
use App\Models\Mysql\ProductUser;
use App\Repositories\BaseRepository;
use Spatie\LaravelData\DataCollection;
use App\DTO\ProductUser\ProductUserDTO;
use Spatie\LaravelData\PaginatedDataCollection;
use Spatie\LaravelData\CursorPaginatedDataCollection;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class ProductUserRepository extends BaseRepository implements ProductUserRepositoryInterface
{

    /**
     * @return void
     */
    protected function setModel(): void
    {
        $this->model = ProductUser::class;
    }

    /**
     * @param \App\DTO\ProductUser\ProductUserDTO $productUser
     *
     * @return \App\DTO\ProductUser\ProductUserDTO
     */
    public function store(
        ProductUserDTO $productUser
    ): ProductUserDTO {
        $transaction = $this->model::query()
            ->create($productUser->toArray());

        $transaction->load(['user', 'product']);

        return ProductUserDTO::from($transaction);
    }

    /**
     * @param \App\DTO\ProductUser\ProductUserDTO $productUser
     *
     * @return \App\DTO\ProductUser\ProductUserDTO
     */
    public function update(
        ProductUserDTO $productUser
    ): ProductUserDTO {
        $model = $this->model::query()
            ->findOrFail($productUser->id);

        $model->update($productUser->toArray());

        $model->load(['product', 'user']);

        return ProductUserDTO::from($model->toArray());
    }

    /**
     * @param int $userId
     * @param int $perPage
     *
     * @return \Spatie\LaravelData\DataCollection|\Spatie\LaravelData\CursorPaginatedDataCollection|\Spatie\LaravelData\PaginatedDataCollection
     */
    public function getBoughtProducts(
        int $userId,
        int $perPage = 10,
    ): DataCollection|CursorPaginatedDataCollection|PaginatedDataCollection {
        $products = $this->model::query()
            ->select([
                'id',
                'product_id',
                'user_id',
                'created_at'
            ])
            ->with([
                'product' => static function (BelongsTo $q) {
                    $q->select('id', 'price', 'name', 'is_active');
                }
            ])
            ->where('rental_time', '=', null)
            ->where('user_id', $userId)
            ->paginate($perPage);

        return ProductUserDTO::collection($products);
    }

    /**
     * @param int $userId
     * @param int $perPage
     *
     * @return \Spatie\LaravelData\DataCollection|\Spatie\LaravelData\CursorPaginatedDataCollection|\Spatie\LaravelData\PaginatedDataCollection
     */
    public function getRentProducts(
        int $userId,
        int $perPage = 10,
    ): DataCollection|CursorPaginatedDataCollection|PaginatedDataCollection {
        $products = $this->model::query()
            ->select([
                'id',
                'product_id',
                'rental_time',
                'user_id',
                'created_at'
            ])
            ->with([
                'product' => static function (BelongsTo $q) {
                    $q->select('id', 'price', 'name', 'is_active');
                }
            ])
            ->where('rental_time', '>=', Carbon::now()->format('Y-m-d H:i:s'))
            ->where('user_id', $userId)
            ->paginate($perPage);

        return ProductUserDTO::collection($products);
    }

    /**
     * @param int $productUserId
     * @param int $rentPeriod
     *
     * @return \App\DTO\ProductUser\ProductUserDTO
     */
    public function findWithProductAndRentPeriodPrice(
        int $productUserId,
        int $rentPeriod
    ): ProductUserDTO {
        $productUser = $this->model::query()
            ->with([
                'product' => static function (BelongsTo $q) use ($rentPeriod) {
                    $q->with([
                        'rentPeriods' => static function (BelongsToMany $q) use ($rentPeriod) {
                            $q->where('value', $rentPeriod)
                                ->select('price as rent_period_price')
                                ->first();
                        }
                    ]);
                },
                'user'
            ])
            ->findOrFail($productUserId);

        $productUser->product->setAttribute(
            'rent_period_price',
            $productUser->product?->rentPeriods?->first()?->rent_period_price
        );

        return ProductUserDTO::from($productUser);
    }

    /**
     * @param int $id
     *
     * @return \App\DTO\ProductUser\ProductUserDTO
     */
    public function findWithProductAndUser(
        int $id
    ): ProductUserDTO {
        $productUser = $this->model::query()
            ->with([
                'product',
                'user'
            ])
            ->lockForUpdate()
            ->findOrFail($id);


        $productUser->code = $productUser->code ?? Str::random(255);
        $productUser->save();

        return ProductUserDTO::from($productUser);
    }

}
