<?php

namespace App\Repositories\ProductUser;

use Spatie\LaravelData\DataCollection;
use App\DTO\ProductUser\ProductUserDTO;
use Spatie\LaravelData\PaginatedDataCollection;
use Spatie\LaravelData\CursorPaginatedDataCollection;

interface ProductUserRepositoryInterface
{
    /**
     * @param \App\DTO\ProductUser\ProductUserDTO $productUser
     *
     * @return \App\DTO\ProductUser\ProductUserDTO
     */
    public function store(
        ProductUserDTO $productUser
    ): ProductUserDTO;

    /**
     * @param \App\DTO\ProductUser\ProductUserDTO $productUser
     *
     * @return \App\DTO\ProductUser\ProductUserDTO
     */
    public function update(
        ProductUserDTO $productUser
    ): ProductUserDTO;

    /**
     * @param int $userId
     * @param int $perPage
     *
     * @return \Spatie\LaravelData\DataCollection|\Spatie\LaravelData\CursorPaginatedDataCollection|\Spatie\LaravelData\PaginatedDataCollection
     */
    public function getBoughtProducts(
        int $userId,
        int $perPage = 10,
    ): DataCollection|CursorPaginatedDataCollection|PaginatedDataCollection;

    /**
     * @param int $userId
     * @param int $perPage
     *
     * @return \Spatie\LaravelData\DataCollection|\Spatie\LaravelData\CursorPaginatedDataCollection|\Spatie\LaravelData\PaginatedDataCollection
     */
    public function getRentProducts(
        int $userId,
        int $perPage = 10,
    ): DataCollection|CursorPaginatedDataCollection|PaginatedDataCollection;

    /**
     * @param int $productUserId
     * @param int $rentPeriod
     *
     * @return \App\DTO\ProductUser\ProductUserDTO
     */
    public function findWithProductAndRentPeriodPrice(
        int $productUserId,
        int $rentPeriod
    ): ProductUserDTO;
}
