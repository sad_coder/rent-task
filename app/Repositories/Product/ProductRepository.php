<?php

namespace App\Repositories\Product;

use App\Models\Mysql\Product;
use App\DTO\Product\ProductDTO;
use App\DTO\Product\ProductRentDTO;
use App\Repositories\BaseRepository;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\LaravelData\DataCollection;
use Spatie\LaravelData\PaginatedDataCollection;
use Spatie\LaravelData\CursorPaginatedDataCollection;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class ProductRepository extends BaseRepository implements ProductRepositoryInterface
{

    /**
     * @return void
     */
    protected function setModel(): void
    {
        $this->model = Product::class;
    }

    /**
     * @param int $perPage
     *
     * @return \Spatie\LaravelData\DataCollection|\Spatie\LaravelData\CursorPaginatedDataCollection|\Spatie\LaravelData\PaginatedDataCollection
     */
    public function listActiveWithPaginate(
        int $perPage = 10
    ): DataCollection|CursorPaginatedDataCollection|PaginatedDataCollection {
        $data = QueryBuilder::for($this->model)
            ->allowedFilters([
                'id',
                'name',
                'price',
            ])
            ->where('is_active', true)
            ->paginate($perPage);

        return ProductDTO::collection($data);
    }

    /**
     * @param int $id
     *
     * @return \App\DTO\Product\ProductDTO
     */
    public function find(
        int $id
    ): ProductDTO {
        $product = $this->model::query()
            ->findOrFail($id);

        return ProductDTO::from($product);
    }

    /**
     * @param int $id
     * @param int $rentPeriod
     *
     * @return \App\DTO\Product\ProductRentDTO
     */
    public function findWithRentPeriod(
        int $id,
        int $rentPeriod
    ): ProductRentDTO {
        $product = $this->model::query()
            ->with(['rentPeriods' => static function (BelongsToMany $q) use ($rentPeriod) {
                $q->where('value', $rentPeriod)
                    ->select( 'price as rent_period_price')
                    ->first();
            }])
            ->findOrFail($id);
        /*
         * здесь мы в наглую добавляем атрибут чтобы в дальнейшем не пришлось мучаться если хотим вытащить цену
         * на аренду того или иного продукта, также добавляем в dto чтобы не было багов если придет null
         * нам dto выдаст ошибку а для клиента обернем в исключение и просто дадим server error
         * возникнуть такая ошибка может только при отсутствии корретных данных
         */

        $product->setAttribute('rent_period_price', $product?->rentPeriods?->first()?->rent_period_price);

        return ProductRentDTO::from($product);
    }

}
