<?php

namespace App\Repositories\Product;

use App\DTO\Product\ProductDTO;
use App\DTO\Product\ProductRentDTO;
use Spatie\LaravelData\DataCollection;
use Spatie\LaravelData\PaginatedDataCollection;
use Spatie\LaravelData\CursorPaginatedDataCollection;

interface ProductRepositoryInterface
{

    /**
     * @param int $perPage
     *
     * @return \Spatie\LaravelData\DataCollection|\Spatie\LaravelData\CursorPaginatedDataCollection|\Spatie\LaravelData\PaginatedDataCollection
     */
    public function listActiveWithPaginate(
        int $perPage = 10
    ): DataCollection|CursorPaginatedDataCollection|PaginatedDataCollection;

    /**
     * @param int $id
     *
     * @return \App\DTO\Product\ProductDTO
     */
    public function find(
        int $id
    ): ProductDTO;

    /**
     * @param int $id
     * @param int $rentPeriod
     *
     * @return \App\DTO\Product\ProductRentDTO
     */
    public function findWithRentPeriod(
        int $id,
        int $rentPeriod
    ): ProductRentDTO;
}
