<?php

namespace App\Repositories\TransactionLog;

use App\Repositories\BaseRepository;
use App\Models\Mysql\TransactionLog;
use App\DTO\TransactionLog\TransactionLogDTO;

class TransactionLogRepository extends BaseRepository implements TransactionLogRepositoryInterface
{

    /**
     * @return void
     */
    protected function setModel(): void
    {
        $this->model = TransactionLog::class;
    }

    /**
     * @param \App\DTO\TransactionLog\TransactionLogDTO $trasaction
     *
     * @return \App\DTO\TransactionLog\TransactionLogDTO
     */
    public function store(
        TransactionLogDTO $trasaction
    ): TransactionLogDTO {

        $transaction = $this->model::query()
            ->create($trasaction->toArray());

        return TransactionLogDTO::from($transaction);
    }

}
