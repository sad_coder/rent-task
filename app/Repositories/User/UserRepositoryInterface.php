<?php

namespace App\Repositories\User;

use App\Models\User;

interface UserRepositoryInterface
{
    /**
     * @param int $id
     *
     * @return \App\Models\User
     */
    public function lockForUpdate(
        int $id
    ): User;

    /**
     * @param \App\Models\User $user
     * @param int              $amount
     *
     * @return void
     */
    public function deductBalance(
        User $user,
        int $amount
    ): void;
}
