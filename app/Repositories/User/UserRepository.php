<?php

namespace App\Repositories\User;

use App\Models\User;
use App\Repositories\BaseRepository;
use App\Exceptions\InsufficientFundsException;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{

    /**
     * @return void
     */
    protected function setModel(): void
    {
        $this->model = User::class;
    }

    /**
     * @param int $id
     *
     * @return \App\Models\User
     */
    public function lockForUpdate(
        int $id
    ): User {
        return $this->model::query()
            ->lockForUpdate()
            ->findOrFail($id);
    }

    /**
     * @param \App\Models\User $user
     * @param int              $amount
     *
     * @return void
     * @throws \App\Exceptions\InsufficientFundsException
     */
    public function deductBalance(
        User $user,
        int $amount
    ): void {
        /*
         * @todo эта проверка сделана на всякий случай
        */
        if ($amount > $user->balance) {
            throw new InsufficientFundsException(
                "insufficient_funds",
                'Недостаточно средств на балансе для выполнения этой операции. Пополните ваш баланс',
            );
        }


        $user->balance -= $amount;
        $user->save();
    }

}
