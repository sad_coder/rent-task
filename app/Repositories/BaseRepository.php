<?php

namespace App\Repositories;

use Spatie\LaravelData\Data;
use Illuminate\Database\Eloquent\Model;

/**
 * Class BaseRepository
 *
 * @package App\Repositories
 */
abstract class BaseRepository
{

    /**
     * @var string|\Illuminate\Database\Eloquent\Model
     */
    protected string|Model $model;

    public function __construct()
    {
        $this->setModel();
    }

    /**
     * @return void
     */
    abstract protected function setModel(): void;
}
