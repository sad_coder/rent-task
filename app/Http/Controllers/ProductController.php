<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\DTO\User\UserDTO;
use Illuminate\Support\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Knuckles\Scribe\Attributes\Group;
use Knuckles\Scribe\Attributes\Header;
use App\Exceptions\ValidationException;
use App\Services\Product\ProductService;
use Knuckles\Scribe\Attributes\UrlParam;
use Knuckles\Scribe\Attributes\Response;
use App\Repositories\User\UserRepository;
use App\Exceptions\InsufficientFundsException;
use App\Repositories\Product\ProductRepository;
use Knuckles\Scribe\Attributes\ResponseFromFile;
use App\Repositories\Product\ProductRepositoryInterface;
use App\Http\Requests\Client\Product\RentProductRequest;

#[Group('Product requests')]
#[Header("Content-Type", "application/json")]
#[Header("Accept-Encoding", "application/json")]
class ProductController extends BaseController
{
    /**
     * @param \App\Repositories\Product\ProductRepositoryInterface $productRepository
     * @param \App\Repositories\User\UserRepository                $userRepository
     * @param \App\Services\Product\ProductService                 $productService
     */
    public function __construct(
        private readonly ProductRepositoryInterface $productRepository,
        private readonly UserRepository $userRepository,
        private readonly ProductService $productService = new ProductService(),
    ) {
    }

    /**
     * @group Product Requests
     *
     * @authenticated
     *
     *
     *
     * @return \Illuminate\Http\JsonResponse
     */
    #[ResponseFromFile("responses/product/indexActive/200.json", 200, description: "Active products list")]
    public function indexActive(): JsonResponse
    {
        $products = $this->productRepository->listActiveWithPaginate();

        return $this->respondWithSuccess(
            $products->toArray(),
            'index products',
            'products'
        );
    }

    /**
     * @param int $productId
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\DatabaseException
     * @throws \App\Exceptions\InsufficientFundsException
     */
    #[UrlParam("productId", "int", "The product's ID.", example: 44)]
    #[ResponseFromFile("responses/product/buy/402.json", 402, description: "Product not active or user don't has money")]
    #[ResponseFromFile("responses/product/buy/200.json", 200, description: "Success buy product")]
    public function buy(
        int $productId
    ): JsonResponse {
        $user = UserDTO::from($this->userRepository->lockForUpdate(Auth::id()));

        $product = $this->productRepository->find($productId);

        $this->productService->checkUserCanBuy(
            user: $user,
            product: $product
        );

        $product = $this->productService->buy(
            $user,
            $product
        );

        return $this->respondWithSuccess(
            $product->toArray(),
            'success buy product',
            'product_user'
        );
    }

    /**
     * @param int                                                  $productId
     * @param \App\Http\Requests\Client\Product\RentProductRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\DatabaseException
     * @throws \App\Exceptions\InsufficientFundsException
     */
    #[UrlParam("productId", "int", "The product's ID.", example: 44)]
    #[ResponseFromFile("responses/product/rent/402.json", 402, description: "Product not active or user don't has money")]
    #[ResponseFromFile("responses/product/buy/200.json", 200, description: "Success buy product")]
    #[ResponseFromFile("responses/product/buy/422.json", 422, description: "Validation errors")]
    public function rent(
        int $productId,
        RentProductRequest $request,
    ): JsonResponse {
        $user = $this->userRepository->lockForUpdate(Auth::id());

        $product = $this->productRepository->findWithRentPeriod(
            id: $productId,
            rentPeriod: $request->validated('rent_time_period')
        );

        $this->productService->checkUserCanRent(
            user: $user,
            product: $product
        );

        $productUser = $this->productService->rent(
            $user,
            $product,
            Carbon::now()->addHours($request->validated('rent_time_period'))
        );

        return $this->respondWithSuccess(
            $productUser->toArray(),
            'success rent product',
            'product_user'
        );
    }
}
