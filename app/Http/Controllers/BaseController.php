<?php

namespace App\Http\Controllers;

use App\Traits\ApiResponseTrait;

class BaseController
{
    use ApiResponseTrait;
}
