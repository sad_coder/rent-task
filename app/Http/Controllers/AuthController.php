<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Http\JsonResponse;
use Knuckles\Scribe\Attributes\Group;
use App\Http\Requests\Client\Auth\LoginRequest;

#[Group('Auth requests')]
class AuthController extends BaseController
{

    /**
     * @param \App\Http\Requests\Client\Auth\LoginRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(
        LoginRequest $request
    ): JsonResponse {
        if (!$auth = auth()->attempt([
            'email' => $request->input('email'),
            'password' => $request->input('password')
        ])) {
            return $this->respondWithAuthorizationError('Unauthorized');
        }

        return $this->respondWithSuccess([
            'access_token' => $auth,
            'token_type' => 'bearer',
            'expires_in' => Carbon::now()->addMinutes(config('jwt.ttl'))
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function me(): JsonResponse
    {
        return $this->respondWithSuccess(
            auth()->user(),
        );
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(): JsonResponse
    {
        auth()->logout();

        return $this->respondWithSuccess([], 'Successfully logged out');
    }

}
