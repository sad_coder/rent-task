<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Knuckles\Scribe\Attributes\Group;
use App\Services\Product\ProductService;
use Knuckles\Scribe\Attributes\UrlParam;
use Knuckles\Scribe\Attributes\BodyParam;
use Knuckles\Scribe\Attributes\ResponseFromFile;
use App\Http\Requests\Client\Product\RenewalRentRequest;
use App\Repositories\ProductUser\ProductUserRepositoryInterface;

#[Group('ProductUser requests')]
class ProductUserController extends BaseController
{

    /**
     * @param \App\Repositories\ProductUser\ProductUserRepositoryInterface $productUserRepository
     * @param \App\Services\Product\ProductService                         $productService
     */
    public function __construct(
        private readonly ProductUserRepositoryInterface $productUserRepository,
        private readonly ProductService $productService = new ProductService(),
    ) {
    }

    /**
     * @authenticated
     *
     * @return \Illuminate\Http\JsonResponse
     */
    #[ResponseFromFile("responses/product-user/userBoughtProducts/200.json", 200, description: "Success index user bought products")]
    public function userBoughtProducts(): JsonResponse
    {
        $products = $this->productUserRepository->getBoughtProducts(
            userId: Auth::id(),
            perPage: 10
        );

        return $this->respondWithSuccess(
            $products->toArray(),
            'Bought user products',
            'bought_products'
        );
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @authenticated
     */
    #[ResponseFromFile("responses/product-user/userRentProducts/200.json", 200, description: "Success index user bought products")]
    public function userRentProducts(): JsonResponse
    {
        $userId = 1;

        $products = $this->productUserRepository->getRentProducts(
            userId: $userId,
            perPage: 10
        );

        return $this->respondWithSuccess(
            $products->toArray(),
            'Rent user products',
            'rent_products'
        );
    }

    /**
     * @param \App\Http\Requests\Client\Product\RenewalRentRequest $request
     * @param int                                                  $productUserId
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\DatabaseException
     *
     * @authenticated
     */
    #[BodyParam("rent_time_period", "enum", "The id of the user.", example: '4,8,12,24')]
    #[UrlParam("productUserId", "int", "ProductUser model id", required: true, example: 13)]
    #[ResponseFromFile("responses/product-user/renewalRent/200.json", 200, description: "Success index user bought products")]
    public function renewalRent(
        RenewalRentRequest $request,
        int $productUserId
    ): JsonResponse {
        $productUser = $this->productUserRepository->findWithProductAndRentPeriodPrice(
            productUserId: $productUserId,
            rentPeriod: $request->validated('rent_time_period')
        );

        $this->productService->checkUserCanRenewalRent(
            productUser: $productUser,
            rentHourPeriod: $request->validated('rent_time_period')
        );

        $productUser = $this->productService->renewalRent(
            productUser: $productUser,
            rentHourPeriod: $request->validated('rent_time_period')
        );

        return $this->respondWithSuccess(
            $productUser->toArray(),
            'renewal user product success',
            'product_user'
        );
    }

    /**
     * @param int $productUserId
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @authenticated
     */
    #[ResponseFromFile("responses/product-user/checkStatus/200.json", 200, description: "Success check user product status")]
    #[ResponseFromFile("responses/product-user/checkStatus/403.json", 403, description: "forbidden product status")]
    public function checkStatus(
        int $productUserId
    ): JsonResponse {
        $productUser = $this->productUserRepository->findWithProductAndUser(
            id: $productUserId
        );

        return $this->respondWithSuccess(
          $productUser->toArray(),
          'check status product user',
            'product_user'
        );
    }

}
