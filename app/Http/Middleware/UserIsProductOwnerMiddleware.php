<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;
use App\Repositories\ProductUser\ProductUserRepository;

class UserIsProductOwnerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response) $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $productUserRepository = new ProductUserRepository();

        $productUser = $productUserRepository->findWithProductAndUser(
            $request->route('productUserId')
        );

        if ($productUser->user_id !== Auth::id()) {
            return response()->json([
                'message' => 'forbidden'
            ], Response::HTTP_FORBIDDEN);
        }


        return $next($request);
    }
}
