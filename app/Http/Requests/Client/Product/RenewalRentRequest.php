<?php

namespace App\Http\Requests\Client\Product;

use App\Http\Requests\BaseRequest;

class RenewalRentRequest extends BaseRequest
{
    /**
     * @return array[]
     */
    public function rules(): array
    {
        return [
            'rent_time_period' => ['in:4,8,12,24', 'required']
        ];
    }
}
