<?php

namespace App\Services\TransactionLog;

use Carbon\Carbon;
use App\DTO\ProductUser\ProductUserDTO;
use App\DTO\TransactionLog\TransactionLogDTO;
use App\Repositories\TransactionLog\TransactionLogRepository;

class TransactionLogService
{
    public function __construct(
        private readonly TransactionLogRepository $transactionLogRepository = new TransactionLogRepository(),
    ) {
    }

    /**
     * @param int $productId
     * @param int $userId
     *
     * @return \App\DTO\TransactionLog\TransactionLogDTO
     */
    public function storeBuyLog(
        int $productId,
        int $userId
    ): TransactionLogDTO {
        return $this->transactionLogRepository->store(
            TransactionLogDTO::from([
                'product_id' => $productId,
                'user_id' => $userId,
                'type' => 'buy',
                'description' => "пользователь $userId купил продукт $productId"
            ])
        );
    }

    /**
     * @param int            $productId
     * @param int            $userId
     * @param \Carbon\Carbon $rentEndTime
     *
     * @return \App\DTO\TransactionLog\TransactionLogDTO
     */
    public function storeRentLog(
        int $productId,
        int $userId,
        Carbon $rentEndTime
    ): TransactionLogDTO {
        $rentEndTime = $rentEndTime->format('Y-m-d H:i:s');

        return $this->transactionLogRepository->store(
            TransactionLogDTO::from([
                'product_id' => $productId,
                'user_id' => $userId,
                'type' => 'rent',
                'description' => "пользователь $userId арендовал продукт $productId до $rentEndTime"
            ])
        );
    }

    /**
     * @param \App\DTO\ProductUser\ProductUserDTO $productUserDTO
     * @param int                                 $rentHourPeriod
     *
     * @return \App\DTO\TransactionLog\TransactionLogDTO
     */
    public function storeRenewalRentLog(
        ProductUserDTO $productUserDTO,
        int $rentHourPeriod
    ): TransactionLogDTO {

        return $this->transactionLogRepository->store(
            TransactionLogDTO::from([
                'product_id' => $productUserDTO->product->id,
                'user_id' => $productUserDTO->user_id,
                'type' => 'rent',
                'description' => "пользователь {$productUserDTO->user_id} продлил аренду продукта {$productUserDTO->product->id}
                    на $rentHourPeriod часов до $productUserDTO->rental_time
                "
            ])
        );
    }

}
