<?php

namespace App\Services\ProductUser;

use Illuminate\Support\Carbon;
use App\DTO\ProductUser\ProductUserDTO;
use App\Repositories\ProductUser\ProductUserRepository;

class ProductUserService
{

    /**
     * @param \App\Repositories\ProductUser\ProductUserRepository $productUserRepository
     */
    public function __construct(
        private readonly ProductUserRepository $productUserRepository = new ProductUserRepository()
    ) {
    }

    /**
     * @param int $productId
     * @param int $userId
     *
     * @return \App\DTO\ProductUser\ProductUserDTO
     */
    public function addBuyProductToUser(
        int $productId,
        int $userId
    ): ProductUserDTO {
        $productUser = $this->productUserRepository->store(
            ProductUserDTO::from([
                'rental_time' => null,
                'product_id' => $productId,
                'user_id' => $userId
            ])
        );
        return ProductUserDTO::from($productUser);
    }

    /**
     * @param int                        $productId
     * @param int                        $userId
     * @param \Illuminate\Support\Carbon $rental_time
     *
     * @return \App\DTO\ProductUser\ProductUserDTO
     */
    public function addRentProductToUser(
        int $productId,
        int $userId,
        Carbon $rental_time
    ): ProductUserDTO {
        $productUser = $this->productUserRepository->store(
            ProductUserDTO::from([
                'rental_time' => $rental_time->format('Y-m-d H:i:s'),
                'product_id' => $productId,
                'user_id' => $userId
            ])
        );
        return ProductUserDTO::from($productUser);
    }

    /**
     * @param \App\DTO\ProductUser\ProductUserDTO $productUser
     * @param string                              $newEndRentalTime
     *
     * @return \App\DTO\ProductUser\ProductUserDTO
     */
    public function renewalProductToUser(
        ProductUserDTO $productUser,
        string $newEndRentalTime
    ): ProductUserDTO {
        $productUser->rental_time = $newEndRentalTime;

        return $this->productUserRepository->update(
            $productUser,
        );
    }

}
