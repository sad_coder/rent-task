<?php

namespace App\Services\Product;

use Exception;
use App\Models\User;
use App\DTO\User\UserDTO;
use Illuminate\Support\Carbon;
use App\DTO\Product\ProductDTO;
use Illuminate\Support\Facades\DB;
use App\DTO\Product\ProductRentDTO;
use App\Exceptions\DatabaseException;
use App\DTO\ProductUser\ProductUserDTO;
use App\Repositories\User\UserRepository;
use App\Services\Product\Checks\CheckContext;
use App\Services\ProductUser\ProductUserService;
use App\Services\Product\Checks\Strategies\UserBalanceForBuyCheck;
use App\Services\TransactionLog\TransactionLogService;
use App\Services\Product\Checks\Strategies\ProductIsActiveCheck;
use App\Services\Product\Checks\Strategies\MaxProductRentTimeCheck;
use App\Services\Product\Checks\Strategies\UserBalanceForRentCheck;

class ProductService
{

    /**
     * @var array
     */
    protected array $checkBuyStrategies = [];

    /**
     * @var array
     */
    protected array $checkRentStrategies = [];

    /**
     * @var array
     */
    protected array $checkRenewalRentStrategies = [];

    public function __construct(
        private readonly TransactionLogService $transactionLogService = new TransactionLogService(),
        private readonly ProductUserService $productUserService = new ProductUserService(),
        private readonly UserRepository $userRepository = new UserRepository(),
    ) {
        /*
         * В дальнейшем при добавлении новых проверок или действий чтобы
         * не делать конструктор монструозным лучше вынести по классам
         * и немного переписать обработчик но пока хорошо выполняет свою
         * задачу
         *
         */

        $this->checkBuyStrategies[] = new UserBalanceForBuyCheck();
        $this->checkBuyStrategies[] = new ProductIsActiveCheck();

        $this->checkRentStrategies[] = new UserBalanceForRentCheck();
        $this->checkRentStrategies[] = new ProductIsActiveCheck();

        $this->checkRenewalRentStrategies[] = new UserBalanceForRentCheck();
        $this->checkRenewalRentStrategies[] = new ProductIsActiveCheck();
        $this->checkRenewalRentStrategies[] = new MaxProductRentTimeCheck();
    }

    /**
     * @param \App\DTO\User\UserDTO       $user
     * @param \App\DTO\Product\ProductDTO $product
     *
     * @return bool
     */
    public function checkUserCanBuy(
        UserDTO $user,
        ProductDTO $product
    ): bool {
        $context = (new CheckContext())
            ->setUser($user)
            ->setProduct($product);

        foreach ($this->checkBuyStrategies as $strategy) {
            if (!$strategy->check($context)) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param \App\DTO\User\UserDTO|\App\Models\User $user
     * @param \App\DTO\Product\ProductRentDTO        $product
     *
     * @return bool
     */
    public function checkUserCanRent(
        UserDTO|User $user,
        ProductRentDTO $product
    ): bool {
        $context = (new CheckContext())
            ->setUser($user)
            ->setProduct($product);

        foreach ($this->checkRentStrategies as $strategy) {
            if (!$strategy->check($context)) {
                return false; // Если одна из проверок не прошла
            }
        }
        return true;
    }

    /**
     * @param \App\DTO\ProductUser\ProductUserDTO $productUser
     * @param int                                 $rentHourPeriod
     *
     * @return void
     */
    public function checkUserCanRenewalRent(
        ProductUserDTO $productUser,
        int $rentHourPeriod
    ): void {
        $context = (new CheckContext())
            ->setUser($productUser->user)
            ->setProduct($productUser->product)
            ->setProductUser($productUser)
            ->setRentHourPeriod($rentHourPeriod);


        /** @var \App\Services\Product\Checks\Interfaces\CheckStrategy $strategy */
        foreach ($this->checkRenewalRentStrategies as $k => $strategy) {
            $strategy->check($context);
        }
    }

    /**
     * @param \App\DTO\User\UserDTO       $user
     * @param \App\DTO\Product\ProductDTO $product
     *
     * @return \App\DTO\ProductUser\ProductUserDTO
     * @throws \App\Exceptions\DatabaseException
     */
    public function buy(
        UserDTO $user,
        ProductDTO $product
    ): ProductUserDTO {
        try {
            DB::beginTransaction();

            //делаем запрос чтобы заблокировать обновление текущей записи пользователя другим инстансом Mysql
            $user = $this->userRepository->lockForUpdate($user->id);

            $this->transactionLogService->storeBuyLog(
                productId: $product->id,
                userId: $user->id
            );

            $productUser = $this->productUserService->addBuyProductToUser(
                productId: $product->id,
                userId: $user->id
            );

            $this->userRepository->deductBalance(
                user: $user,
                amount: $product->price
            );

            DB::commit();
            return $productUser;
        } catch (Exception $e) {
            DB::rollBack();

            logger($e->getMessage());
            throw new DatabaseException('Server error', $e->getMessage());
        }
    }

    /**
     * @param \App\DTO\User\UserDTO|\App\Models\User $user
     * @param \App\DTO\Product\ProductRentDTO        $product
     * @param \Illuminate\Support\Carbon             $rentEndTime
     *
     * @return \App\DTO\ProductUser\ProductUserDTO
     * @throws \App\Exceptions\DatabaseException
     */
    public function rent(
        UserDTO|User $user,
        ProductRentDTO $product,
        Carbon $rentEndTime
    ): ProductUserDTO {
        try {
            DB::beginTransaction();
            $user = $this->userRepository->lockForUpdate($user->id);

            $this->transactionLogService->storeRentLog(
                productId: $product->id,
                userId: $user->id,
                rentEndTime: $rentEndTime,
            );

            $productUser = $this->productUserService->addRentProductToUser(
                productId: $product->id,
                userId: $user->id,
                rental_time: $rentEndTime
            );

            $this->userRepository->deductBalance(
                user: $user,
                amount: $product->rent_period_price
            );

            DB::commit();
            return $productUser;
        } catch (Exception $e) {
            DB::rollBack();
            logger($e->getMessage());
            throw new DatabaseException('Server error', $e->getMessage());
        }
    }

    /**
     * @param \App\DTO\ProductUser\ProductUserDTO $productUser
     * @param int                                 $rentHourPeriod
     *
     * @return ProductUserDTO
     * @throws \App\Exceptions\DatabaseException
     */
    public function renewalRent(
        ProductUserDTO $productUser,
        int $rentHourPeriod,
    ): ProductUserDTO {
        try {
            $user = $this->userRepository->lockForUpdate($productUser->user_id);

            $this->productUserService->renewalProductToUser(
                productUser: $productUser,
                newEndRentalTime: Carbon::parse($productUser->rental_time)
                    ->addHours($rentHourPeriod),
            );

            $this->transactionLogService->storeRenewalRentLog(
                productUserDTO: $productUser,
                rentHourPeriod: $rentHourPeriod
            );

            $this->userRepository->deductBalance(
                user: $user,
                amount: $productUser->product->rent_period_price
            );
            return ProductUserDTO::from($productUser);
        } catch (Exception $e) {
            logger($e->getMessage());
            throw new DatabaseException('Server error', $e->getMessage());
        }
    }

}
