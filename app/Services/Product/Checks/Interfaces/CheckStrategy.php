<?php

namespace App\Services\Product\Checks\Interfaces;

use App\Models\User;
use App\DTO\Product\ProductDTO;
use App\Services\Product\Checks\CheckContext;

interface CheckStrategy
{
    /**
     * @param \App\Services\Product\Checks\CheckContext $checkContext
     *
     * @return void
     */
    public function check(CheckContext $checkContext): void;
}
