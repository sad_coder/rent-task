<?php

namespace App\Services\Product\Checks\Strategies;

use App\Services\Product\Checks\CheckContext;
use App\Exceptions\InsufficientFundsException;
use App\Services\Product\Checks\Interfaces\CheckStrategy;

class UserBalanceForRentCheck implements CheckStrategy
{

    /**
     * @param \App\Services\Product\Checks\CheckContext $checkContext
     *
     * @return void
     * @throws \App\Exceptions\InsufficientFundsException
     */
    public function check(
        CheckContext $checkContext
    ): void {

        if ($checkContext->getUser()->balance < $checkContext->getProduct()->rent_period_price) {
            throw new InsufficientFundsException('insufficient_funds');
        }

    }
}
