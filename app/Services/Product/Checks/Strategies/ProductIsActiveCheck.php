<?php

namespace App\Services\Product\Checks\Strategies;

use App\Models\User;
use App\DTO\Product\ProductDTO;
use App\Services\Product\Checks\CheckContext;
use App\Exceptions\ProductNotActiveException;
use App\Services\Product\Checks\Interfaces\CheckStrategy;

class ProductIsActiveCheck implements CheckStrategy
{

    /**
     *
     * @param \App\Services\Product\Checks\CheckContext $checkContext
     *
     * @return void
     * @throws \App\Exceptions\ProductNotActiveException
     */
    public function check(
        CheckContext $checkContext
    ): void {
        if (!$checkContext->getProduct()->is_active) {
            throw new ProductNotActiveException('product_not_active');
        }
    }
}
