<?php

namespace App\Services\Product\Checks\Strategies;

use Carbon\Carbon;
use App\Models\User;
use App\DTO\Product\ProductDTO;
use App\Services\Product\Checks\CheckContext;
use App\Exceptions\MaxProductRentTimeException;
use App\Services\Product\Checks\Interfaces\CheckStrategy;

class MaxProductRentTimeCheck implements CheckStrategy
{

    /**
     * @param \App\Services\Product\Checks\CheckContext $checkContext
     *
     * @return void
     * @throws \App\Exceptions\MaxProductRentTimeException
     */
    public function check(CheckContext $checkContext): void
    {
        $newRentalTime = Carbon::parse($checkContext->getProductUser()->rental_time)
            ->addHours($checkContext->getRentHourPeriod());

        $hoursDifference = (Carbon::now())->diffInHours($newRentalTime);

        if ($hoursDifference > 24) {
            throw new MaxProductRentTimeException('max_product_rent_time');
        }
    }
}
