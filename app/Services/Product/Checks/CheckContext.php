<?php

namespace App\Services\Product\Checks;

/*
 * В зависимости от вида проверки понадобится разный контекс и разные аргументы и параметры
 *
 */

use App\Models\User;
use App\DTO\User\UserDTO;
use App\DTO\Product\ProductDTO;
use App\DTO\Product\ProductRentDTO;
use App\DTO\ProductUser\ProductUserDTO;

class CheckContext
{
    /**
     * @var \App\DTO\User\UserDTO|\App\Models\User
     */
    protected UserDTO|User $user;

    /**
     * @var \App\DTO\Product\ProductDTO|\App\DTO\Product\ProductRentDTO
     */
    protected ProductDTO|ProductRentDTO $product;

    /**
     * @var \App\DTO\ProductUser\ProductUserDTO
     */
    protected ProductUserDTO $productUser;

    /**
     * @var int
     */
    protected int $rentHourPeriod;

    /**
     * @param \App\DTO\User\UserDTO|\App\Models\User $user
     *
     * @return \App\Services\Product\Checks\CheckContext
     */
    public function setUser(UserDTO|User $user): self
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @param int $rentHourPeriod
     *
     * @return $this
     */
    public function setRentHourPeriod(int $rentHourPeriod): self
    {
        $this->rentHourPeriod = $rentHourPeriod;
        return $this;
    }

    /**
     * @param \App\DTO\ProductUser\ProductUserDTO $productUser
     *
     * @return \App\Services\Product\Checks\CheckContext
     */
    public function setProductUser(ProductUserDTO $productUser): self
    {
        $this->productUser = $productUser;
        return $this;
    }

    /**
     * @param \App\DTO\Product\ProductDTO|\App\DTO\Product\ProductRentDTO $product
     *
     * @return \App\Services\Product\Checks\CheckContext
     */
    public function setProduct(ProductDTO|ProductRentDTO $product): self
    {
        $this->product = $product;
        return $this;
    }

    /**
     * @return \App\DTO\Product\ProductDTO|\App\DTO\Product\ProductRentDTO
     */
    public function getProduct(): ProductDTO|ProductRentDTO
    {
        return $this->product;
    }

    /**
     * @return \App\DTO\User\UserDTO|\App\Models\User
     */
    public function getUser(): UserDTO|User
    {
        return $this->user;
    }

    /**
     * @return \App\DTO\ProductUser\ProductUserDTO
     */
    public function getProductUser(): ProductUserDTO
    {
        return $this->productUser;
    }

    public function getRentHourPeriod(): int
    {
        return $this->rentHourPeriod;
    }

}
