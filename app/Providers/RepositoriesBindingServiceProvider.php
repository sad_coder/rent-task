<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\User\UserRepository;
use App\Repositories\Product\ProductRepository;
use App\Repositories\User\UserRepositoryInterface;
use App\Repositories\ProductUser\ProductUserRepository;
use App\Repositories\Product\ProductRepositoryInterface;
use App\Repositories\TransactionLog\TransactionLogRepository;
use App\Repositories\ProductUser\ProductUserRepositoryInterface;
use App\Repositories\TransactionLog\TransactionLogRepositoryInterface;

class RepositoriesBindingServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $repositories = [
            ProductRepositoryInterface::class => ProductRepository::class,
            ProductUserRepositoryInterface::class => ProductUserRepository::class,
            TransactionLogRepositoryInterface::class => TransactionLogRepository::class,
            UserRepositoryInterface::class => UserRepository::class,
        ];

        foreach ($repositories as $interface => $implementation) {
            $this->app->bind($interface, $implementation);
        }
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
