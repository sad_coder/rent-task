<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use App\Models\Mysql\RentPeriod;

class RentPeriodSeeder extends Seeder
{
    public function run(): void
    {
        /*
         *  @todo По хорошему надо бы сделать разные периоды аренды по типу час, сутки,неделя или месяц
         *  но в тз указали только эти периоды
         *  также я бы мог ввести вместо name slug и добавить ключи в переводы
         */

        $defaultRentPeriods = [
            [
                'name' => '4 часа',
                'value' => 4,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => '8 часов',
                'value' => 8,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => '12 часов',
                'value' => 12,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => '24 часа',
                'value' => 12,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]
        ];
        RentPeriod::query()->insertOrIgnore($defaultRentPeriods);
    }
}
