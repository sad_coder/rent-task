<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Mysql\Product;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        User::factory(50)->create();
        Product::factory(100)->create();

        $this->call([
            RentPeriodSeeder::class
        ]);

    }
}
