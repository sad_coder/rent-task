<?php

namespace Database\Seeders;

use App\Models\Mysql\Product;
use Illuminate\Database\Seeder;
use App\Models\Mysql\RentPeriod;

class AddRentPeriodsToProductSeeder extends Seeder
{
    /**
     * @return void
     * @throws \Random\RandomException
     */
    public function run(): void
    {
        $products = Product::query()->get();
        $periods = RentPeriod::query()->get();

        foreach ($products as $product) {
            foreach ($periods as $period) {
                $product->rentPeriods()->attach($period->id, ['price' => random_int(10, 1000)]);
            }
        }
    }
}
