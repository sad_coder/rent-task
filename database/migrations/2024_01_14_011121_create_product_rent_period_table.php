<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('product_rent_period', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('price');

            $table->foreignId('product_id')
                ->references('id')
                ->on('products');

            $table->foreignId('rent_period_id')
                ->references('id')
                ->on('rent_periods');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('product_rent_period', static function (Blueprint $table) {
            $table->dropForeign(['product_id']);
            $table->dropForeign(['rent_period_id']);
        });

        Schema::dropIfExists('product_rent_period');
    }
};
