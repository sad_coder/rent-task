<?php

namespace Database\Factories\Mysql;

use App\DTO\Product\ProductDTO;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return ProductDTO::from([
            'name' => fake()->word(),
            'price' => rand(100,1000),
            'is_active' => rand(0,1),
        ])->toArray();
    }
}
