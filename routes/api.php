<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProductUserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/
Route::group(['middleware' => 'auth'], static function () {
    Route::group(['prefix' => 'product'], static function () {
        Route::get('/', [ProductController::class, 'indexActive']);
        Route::post('buy/{id}', [ProductController::class, 'buy'])
            ->where('id', '[0-9]+');
        Route::post('rent/{id}', [ProductController::class, 'rent'])
            ->where('id', '[0-9]+');
    });


    Route::group(['prefix' => 'user'], static function () {
        Route::get('bought/products', [ProductUserController::class, 'userBoughtProducts']);
        Route::get('rent/products', [ProductUserController::class, 'userRentProducts']);

        Route::post('renewal-rent/{productUserId}', [ProductUserController::class, 'renewalRent'])
            ->where('productUserId', '[0-9]+');

        Route::get('/product/{productUserId}', [ProductUserController::class, 'checkStatus'])
            ->middleware(['is.product.owner']);
    });


    Route::group(['prefix' => 'auth'], static function () {
        Route::post('me', [AuthController::class, 'me']);
    });
});

Route::post('auth/login', [AuthController::class, 'login']);






