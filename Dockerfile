FROM ubuntu:20.04

RUN cat /etc/os-release

ENV TZ=Asia/Almaty
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update \
    && apt-get upgrade -yq \
    && apt-get install -yq --no-install-recommends \
        apt-utils \
        apt-transport-https \
        ca-certificates \
        curl \
        software-properties-common \
    && LC_ALL=C.UTF-8 add-apt-repository ppa:ondrej/php \
    && apt-get remove --purge -y software-properties-common

RUN apt-get update \
    && apt-get install -yq --no-install-recommends \
        php8.1 \
        php8.1-fpm \
        php8.1-bcmath \
        php8.1-bz2 \
        php8.1-exif \
        php8.1-imap \
        php8.1-intl \
        php8.1-ldap \
        php8.1-mbstring \
        php8.1-pdo \
        php8.1-pdo-mysql \
        php8.1-dom \
        php8.1-gd \
        php8.1-zip \
        php8.1-soap \
        php8.1-xdebug \
        php8.1-imagick \
        php8.1-curl \
        php8.1-redis \
        php8.1-opcache \
        git \
        mysql-client \
        graphicsmagick \
        imagemagick \
        ghostscript \
        unzip



RUN apt-get install -y locales
RUN apt-get install -y supervisor
RUN apt-get install -y nginx
RUN apt-get install -y htop

#memcahed
RUN apt-get install -y php8.1-memcached

# Install Composer
COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer

#memcached

RUN locale-gen ru_RU.UTF-8 && update-locale LC_ALL=ru_RU.UTF-8 LANG=ru_RU.UTF-8

RUN rm -Rf /etc/nginx/sites-enabled/*.*
RUN unlink /etc/nginx/sites-enabled/default

COPY build-local/nginx/containers/container-rent-service.conf /etc/nginx/sites-enabled/rent-service.conf

COPY build-local/supervisord.conf /etc/supervisor/conf.d/

RUN service php8.1-fpm start
RUN service nginx start

WORKDIR /var/www/rent-service

EXPOSE 8052

CMD ["/usr/bin/supervisord"]
