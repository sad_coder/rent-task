#!/bin/bash
declare -a services=(\
"rent"
)
export DATABASE_PASSWORD='core-pass'
export DATABASE_HOST='mysqlRentService'

export MYSQL_CONTAINER="mysqlRentService"

# Параметры для подключения к MySQL
export DATABASE_USER="root"

# Имя базы данных, которую нужно создать
export DATABASE_NAME="rent-db"

# Команда для создания базы данных
export CREATE_DATABASE_COMMAND="CREATE DATABASE IF NOT EXISTS \`$DATABASE_NAME\`;"

sudo docker-compose exec "$MYSQL_CONTAINER" mysql -u"$DATABASE_USER" -p"$DATABASE_PASSWORD" -e "$CREATE_DATABASE_COMMAND"

for service in "${services[@]}"; do

     sudo docker-compose exec "$service-service" composer install
     sudo docker-compose exec "$service-service" mkdir "./bootstrap/cache"
     sudo docker-compose exec "$service-service" cp ./.env.example .env

     sudo docker-compose exec "$service-service" sed -i 's/^\(DB_PASSWORD\s*=\s*\).*$/\1'$DATABASE_PASSWORD'/' ./.env
     sudo docker-compose exec "$service-service" sed -i 's/^\(DB_USERNAME\s*=\s*\).*$/\1root/' ./.env
     sudo docker-compose exec "$service-service" sed -i 's/^\(DB_HOST\s*=\s*\).*$/\1'$DATABASE_HOST'/' ./.env

     sudo docker-compose exec "$service-service" sed -i 's/^\(DB_DATABASE\s*=\s*\).*$/\1'"$service-db"'/' ./.env

     sudo docker-compose exec "$service-service" php artisan key:generate

     sudo docker-compose exec "$service-service" php artisan migrate:fresh --seed

     sudo docker-compose exec "$service-service" php artisan scribe:generate
     sudo docker-compose exec "$service-service" php artisan jwt:secret
done
